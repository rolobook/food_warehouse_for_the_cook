-- public."Cook" definition

-- Drop table
DROP TABLE IF  exists public."ChangesProducts";
DROP TABLE IF  exists public."Products";
DROP TABLE IF  exists public."Category";
DROP TABLE IF  exists public."Warehouse";
DROP TABLE IF  exists public."Cook";
--DROP TABLE public."Warehouse";

--CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

 CREATE TABLE IF NOT exists public."Cook"    (

"Id" uuid NOT NULL DEFAULT uuid_generate_v4(),
"CreatedOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),
"ModifiedOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),
"Name" varchar(250) NOT NULL  UNIQUE,
"Email" varchar(250) NOT NULL  UNIQUE,
"Phone" varchar(250) NOT NULL  UNIQUE,
"isDelete" bool NOT NULL DEFAULT false,
"DeleteOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),

PRIMARY KEY ("Id")

);

--склад
CREATE TABLE IF NOT EXISTS public."Warehouse" (

"Id" uuid NOT NULL DEFAULT uuid_generate_v4(),
"CookiId" uuid NOT null,
"CreatedOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),
"ModifiedOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),
"Name" varchar(250) ,
"Address" varchar(250) NULL,
"isDelete" bool NOT NULL DEFAULT false,
"DeleteOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),

FOREIGN KEY ("CookiId") REFERENCES public."Cook"("Id"),
PRIMARY KEY ("Id")

);

-- категории
CREATE TABLE IF NOT exists  public."Category" (

"Id" uuid NOT NULL DEFAULT uuid_generate_v4(),
"CreatedOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),
"ModifiedOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),
"Name" varchar(250) UNIQUE,
"isDelete" bool NOT NULL DEFAULT false,
"DeleteOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),

PRIMARY KEY ("Id")

);


-- продукы
CREATE TABLE IF NOT EXISTS public."Products" (

"Id" uuid NOT NULL DEFAULT uuid_generate_v4(),
"WarehouseId" uuid NOT null,
"CategoryId" uuid NOT null,
"CreatedOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),
"ModifiedOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),
"Name" varchar(250) NOT null UNIQUE,
"ExpirationDate" timestamp null,  -- срок годности
"Quantity" numeric(18, 2) NOT NULL DEFAULT 0, -- количество
"UnitOfMeasure" varchar(250) NOT null, --еденицы измерения
"isDelete" bool NOT NULL DEFAULT false,
"DeleteOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),

FOREIGN KEY ("WarehouseId") REFERENCES public."Warehouse"("Id"),
FOREIGN KEY ("CategoryId") REFERENCES public."Category"("Id"),
PRIMARY KEY ("Id")

);

-- изменения продуктов
CREATE TABLE IF NOT EXISTS public."ChangesProducts" (

"Id" uuid NOT NULL DEFAULT uuid_generate_v4(),
"ProductId" uuid NOT null,
"CreatedOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),
"Quantity" integer NOT NULL DEFAULT 0, -- количество
"UnitOfMeasure" varchar(250) NOT null, --еденицы измерения

FOREIGN KEY ("ProductId") REFERENCES public."Products"("Id"),
PRIMARY KEY ("Id")

);


-- Рецепт
CREATE TABLE IF NOT EXISTS public."Recipe" (

"Id" uuid NOT NULL DEFAULT uuid_generate_v4(),
"ProductId" uuid NOT null,
"CreatedOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),
"ModifiedOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),
"Name" varchar(250) UNIQUE,
"Description" varchar null,

FOREIGN KEY ("ProductId") REFERENCES public."Products"("Id"),
PRIMARY KEY ("Id")

);

--ingredients
CREATE TABLE IF NOT EXISTS public."Ingredients" (

"Id" uuid NOT NULL DEFAULT uuid_generate_v4(),
"ProductId" uuid NOT null,
"RecipeId" uuid NOT null,
"CreatedOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),
"ModifiedOn" timestamp NULL DEFAULT timezone('utc'::text, CURRENT_TIMESTAMP),
"Quantity" integer NOT NULL DEFAULT 0, -- количество
"UnitOfMeasure" varchar(250) NOT null, --еденицы измерения

FOREIGN KEY ("ProductId") REFERENCES public."Products"("Id"),
FOREIGN KEY ("RecipeId") REFERENCES public."Recipe"("Id"),
PRIMARY KEY ("Id")

);
